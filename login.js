module.exports = async function(context, commands) {
	// Pass in Username and Password via CLI, e.g. --browsertime.my.password

	await commands.navigate(
		'https://gitlab.com/users/sign_in'
	);

	try {
		await commands.addText.byId(context.options.my.username, 'user_login')
		await commands.addText.byId(context.options.my.password, 'user_password')

		await commands.measure.start('Login');

		await commands.click.byXpathAndWait('/html/body/div[1]/div[2]/div/div[3]/div[2]/div/div[1]/div[1]/div/form/div[5]/input')

		return commands.measure.stop();
	} catch (e) {
		throw e;
	}

};