module.exports = async function(context, commands) {

	await commands.navigate(
		'https://gitlab.com/gitlab-org/gitlab/-/merge_requests/29128'
	);

	try {

		await commands.measure.start('Pipeline - Expand MR minigraph and open a Job');
		await commands.click.byXpath('/html/body/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div/section/div/div[2]/div[2]/div/div/div/div[2]/span/span/div[4]/div/button');
		await commands.wait.byXpath('/html/body/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div/section/div/div[2]/div[2]/div/div/div/div[2]/span/span/div[4]/div/div/ul/li[1]/div/a', 10000)
		await commands.click.byXpathAndWait('/html/body/div[1]/div[2]/div[3]/div/div[2]/div[2]/div[3]/div[2]/div/section/div/div[2]/div[2]/div/div/div/div[2]/span/span/div[4]/div/div/ul/li[1]/div/a');
		return commands.measure.stop();

	} catch (e) {
		throw e;
	}

};


