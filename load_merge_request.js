module.exports = async function(context, commands) {

	await commands.navigate(
		'https://gitlab.com/gitlab-org/gitlab/'
	);

	try {

		await commands.measure.start('Merge Requests - List');
		await commands.click.byXpathAndWait('/html/body/div/div[1]/div/ul/li[4]/a');
		await commands.measure.stop();

		await commands.addText.byId('Initiate Jira import on frontend', 'filtered-search-merge_requests')

		await commands.measure.start('Merge Requests - Text search');
		await commands.click.byIdAndWait('state-merged');
		await commands.measure.stop();

		await commands.measure.start('Merge Requests - Load large MR');
		await commands.click.byXpathAndWait('/html/body/div/div[2]/div[3]/div/div[4]/ul/li/div/div[1]/div[1]/span[1]/a');
		return commands.measure.stop();

	} catch (e) {
		throw e;
	}

};